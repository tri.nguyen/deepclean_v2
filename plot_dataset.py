from __future__ import print_function

import os
import pickle
import argparse

import matplotlib as mpl
import matplotlib.pyplot as plt

from deepclean import plot_utils, data_utils

mpl.rc('font', size=15)
mpl.rc('figure', figsize=(8, 5))

if __name__ == '__main__':
    # parse cmd argument
    def parse_cmd():
        parser = argparse.ArgumentParser()
        parser.add_argument(
            "--in_file", "-i", help="pickle file to read", 
            required=True, type=str)
        parser.add_argument(
            "--x_low", "-xl", help="lower limit of x axis", 
            dest="xl", type=float)
        parser.add_argument(
            "--x_high", "-xh", help="upper limit of x axis", 
            dest="xh", type=float)
        parser.add_argument(
            "--y_low", "-yl", help="lower limit of y axis", 
            dest="yl", type=float)
        parser.add_argument(
            "--y_high", "-yh", help="upper limit of y axis", 
            dest="yh", type=float)
        parser.add_argument(
            "--whiten", "-w", help="path to whitenning file", type=str)
        params = parser.parse_args()
        return params
    params = parse_cmd()
    
    # import data from pickle file
    with open(params.in_file, 'rb') as f:
        data = pickle.load(f, encoding='latin')
        _, train = data.get('train')
        _, val = data.get('val')
        norm = data.get('norm')
        fs = data.get('fs')
        
    # denormalize if norm is given
    if norm:
        train = data_utils.denormalize(train, *norm['y'])
        val = data_utils.denormalize(val, *norm['y'])
    
    # plot
    fig, ax = plt.subplots(1)
    
    plot_utils.plot_psd(train, ax, whiten=params.whiten, fs=fs, nperseg=4096, label='Train')
    plot_utils.plot_psd(val, ax, whiten=params.whiten, fs=fs, nperseg=4096, label='Val')
    ax.set(xlim=[params.xl, params.xh], ylim=[params.yl, params.yh], 
           xlabel=r'Frequency [$Hz$]', ylabel=r'PSD [$m\;Hz^{-1/2}$]')
    ax.grid(ls='--', which='both')
    ax.legend()
    
    out_file = 'dataset.png'
    print('saving figure to %s.....' % out_file)
    plt.savefig('dataset.png', dpi=100, bbox_inches='tight')

        