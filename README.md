


# DeepClean

DeepClean is a deep learning noise filtering framework desgined to
subtract nonlinear noise coupling mechanisms in gravitational-wave
detectors at the Laser Interferometer Gravitational-wave Observatory
(LIGO). This code is built using [PyTorch](https://pytorch.org/). 

<!-- toc -->

- [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Installation](#installation)
- [Example](#example)
- [Authors](#authors)
- [License](#license)

<!-- tocstop-->

# Getting Started
## Prerequisites
Dependencies:

- [python](https://www.python.org/) (version >= 3.5.2)
- [numpy](http://www.numpy.org) (version >= 1.13.1)
- [scipy](https://github.com/scipy/scipy) (version >= 0.16.0)
- [torch](https://pytorch.org/) (version == 1.0.0)
- [gwpy](https://gwpy.github.io/)
- [nds2](https://www.lsc-group.phys.uwm.edu/daswg/wiki/NetworkDataServer2)
- [framecpp](https://anaconda.org/conda-forge/ldas-tools-framecpp)

Optional dependencies:

- [torchsummary](https://github.com/sksq96/pytorch-summary) (version >= 0.3.0)
- [matplotlib](https://matplotlib.org/)  (version >1.4.1)

## Installation
To avoid any possible package conflicts, it is strongly encouraged to create a clean `conda` environment. `conda` installation instructions can be found at this [link](https://docs.conda.io/projects/conda/en/latest/user-guide/install/). The new environment can be created as follows:
```
git clone https://git.ligo.org/tri.nguyen/deepclean_v2.git deepclean
cd deepclean
conda create -n deepclean_env python=3.6
conda activate deepclean_env
pip install -e .
conda install -c conda-forge python-nds2-client ldas-tools-framecpp
```

For easy installation, one may also run:
```
$ conda env create -f environment.yml
```
The first line of the yml file sets the new environment's name. 
It is, by default, set to `deepclean_env`. Replace `deepclean_env`
with your environment name of choice.

To activate your environment, simply run:
```
conda activate deepclean_env
```

# Example
The simplest way to run DeepClean is with the command `deepclean-silencio`. `deepclean-silencio` takes in a requested time window, a configuration file with the network hyperparameters, and a channel list and outputs a clean GW frame of the requested window. The channel list should have the GW channel on the first line followed by the auxilliary (or noise) channels, each on a different line.  

Suppose you would like to use DeepClean to subtract the non-stationary SRCL noise coupling at LIGO Hanford  from gps time 1224509400 to 1224509880 (more information about the SRCL noise can be found [here](https://alog.ligo-wa.caltech.edu/aLOG/index.php?callRep=45823)) with the configuration [config.ini](https://git.ligo.org/tri.nguyen/deepclean_v2/blob/master/config.ini) and the channel list [chanslist.ini](https://git.ligo.org/tri.nguyen/deepclean_v2/blob/master/chanslist.ini), simply run:
```
$ deepclean-silencio -t0 1224509400 -t1 1224509880 -tt0 1224509280 \
-tt1 1224509400 -i config.ini -c chanslist.ini -o run_0 \
-d Hanford -fs 1024 -l log.txt -g -p 
```
In this example, DeepClean will train the network on the gps time window from 1224509280 to 1224509400 using any available GPU (enable by option `g`). After training, it will perform the subtraction and store the clean GW frame in the working directory `run_0` as `clean.gwf`. The log file (option `l`) and history plot (option `p`) are also stored in `run_0` as `log.txt` and `history.png`. The sampling rate of the output is set to 1024 Hz using option `-fs`.

# Authors
For questions, bug reporting or requests about functionalities to include, please email the maintainer of the repository [Tri Nguyen](tri.nguyen@ligo.org).

DeepClean Group:

- [Tri Nguyen](tri.nguyen@ligo.org)
- [Michael Coughlin](michael.coughlin@ligo.org)
- [Rich Ormiston](rich.ormiston@ligo.org)
- [Rana Adhikari](rana.adhikari@ligo.org)
- [Gabriele Vajente](gabriele.vajente@ligo.org)

# License
Copyright (C) DeepClean Group (2019)

DeepClean is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

DeepClean is distributed in the hope that it will be useful, but without any warranty; without even the implied warranty of merchantability or fitness for a particular purpose.  See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with DeepClean. If not, see <http://www.gnu.org/licenses/>.
