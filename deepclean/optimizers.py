
import torch.optim 

optimizers = {
    'SGD': torch.optim.SGD,
    'RPROP': torch.optim.Rprop,
    'RMSPROP': torch.optim.RMSprop,
    'ASGD': torch.optim.ASGD,
    'ADAMAX': torch.optim.Adamax,
    'SPARSEADAM': torch.optim.SparseAdam,
    'ADAM': torch.optim.Adam,
    'ADAGRAD': torch.optim.Adagrad,
    'ADADELTA': torch.optim.Adadelta
}

def get_optimizer(params):
    ''' Get torch optimizer '''
    optimizer = params.pop('optimizer')
    optim = optimizers[optimizer.upper()]
    return optim(**params)