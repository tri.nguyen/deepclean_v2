
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

# plt.style.use('ggplot')

mpl.rc('font', size=15)
mpl.rc('figure', figsize=(8, 5))

import scipy.signal as sig
from scipy.interpolate import interp1d

from .gw import get_canonical_range


def plot_psd(x, ax=None, fs=1.0, nperseg=256, average='mean', whiten=False, loglog=True, asd=True, **kargs):
    # calculate the psd
    f, p = sig.welch(x.reshape(-1, ), fs=fs, nperseg=nperseg, average=average)
    if asd:
        p = np.sqrt(p)

    # whiten data if given whiten file
    if whiten:
        f_white, p_white_r, p_white_i = np.genfromtxt(whiten, unpack=True)
        p_white = np.abs(p_white_r + 1j*p_white_i)
        p = p / interp1d(f_white, p_white)(f)

    # Plot
    if ax is None:
        fig, ax = plt.subplots(1)
    if loglog: 
        ax.loglog(f, p, **kargs)
    else: 
        ax.plot(f, p, **kargs)

    # return the freq and psd
    return f, p


def plot_before_after(
    hoft, hoft_clean, axes=None, fs=1.0, nperseg=256, average='mean', 
    whiten=False, loglog=True, asd=True, colors=('k', 'b'), 
    labels=('HOFT','Cleaned HOFT'), legend=True):
    
    if axes is None:
        fig, (ax1, ax2) = plt.subplots(
            nrows=2, sharex=True, figsize=(10, 6), 
            gridspec_kw={'height_ratios': [2, 1]}
        )
    else:
        ax1, ax2 = axes

    # Plot ASD
    freq, asd_original = plot_psd(
        hoft, ax1, fs, nperseg, average, whiten, loglog, asd, 
        color=colors[0], label=labels[0])
    freq, asd_clean = plot_psd(
        hoft_clean, ax1, fs, nperseg, average, whiten, loglog, asd, 
        color=colors[1], label=labels[1])

    # Plot ASD ratio
    ax2.plot(freq, asd_clean/asd_original, color=colors[1])
    ax2.axhline(1, color='k', ls='--')

    ax1.set_ylabel('ASD')
    ax2.set_xlabel('Frequency [Hz]')
    ax2.set_ylabel('ASD Ratio')

    if legend:
        ax1.legend()
    
    if axes is None:
        return fig, (ax1, ax2)
    
def plot_before_after_range(
    hoft, hoft_cleaned, axes=None, fs=1.0, gps_start=0., 
    nperseg=256, npersample=1024, average='mean', colors=('k', 'b'), 
    labels=('Before', 'After'), plot_volume=True):
    
    if axes is None:
        fig, (ax1, ax2) = plt.subplots(
            nrows=2, sharex=True, figsize=(10, 6), 
            gridspec_kw={'height_ratios': [2, 1]}
        )
    else:
        ax1, ax2 = axes

    nsample = int(len(hoft)/npersample)
    
    # calculate range
    data = np.array(np.array_split(hoft, nsample))
    data_cleaned = np.array(np.array_split(hoft_cleaned, nsample))
    time = np.linspace(gps_start, gps_start + len(hoft)/fs, nsample)

    freq, psd = sig.welch(data, fs=fs, nperseg=nperseg, average=average)
    freq, psd_cleaned = sig.welch(data_cleaned, fs=fs, nperseg=nperseg, average=average)

    d = get_canonical_range(psd, freq, 'bns')
    d_cleaned = get_canonical_range(psd_cleaned, freq, 'bns')
    d_frac_increase = (d_cleaned - d)/d
    V_frac_increase = (d_cleaned**3 - d**3)/d**3
    
    ax1.plot(time, d, label=labels[0], color=colors[0])
    ax1.plot(time, d_cleaned, label=labels[1], color=colors[1])
    ax2.plot(time, d_frac_increase*100, color=colors[1])
    
    if plot_volume:
        ax2twin = ax2.twinx()
        ax2twin.plot(time, V_frac_increase*100)
        ax2twin.set_ylabel('Volume Increase [%]', fontsize=14)

    ax1.set_ylabel('Inspiral Range [Mpc]')
    ax2.set_xlabel('GPS Time [sec]')
    ax2.set_ylabel('Range Increase [%]', fontsize=14)    
    ax1.legend()
    
    if axes is None and plot_volume:
        return fig, (ax1, ax2, ax2twin)
    elif axes is None:
        return fig, (ax1, ax2)