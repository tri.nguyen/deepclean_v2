
import numpy as np
import scipy.signal as sig

import bilby
from bilby.gw.conversion import convert_to_lal_binary_black_hole_parameters
from bilby.gw.conversion import convert_to_lal_binary_neutron_star_parameters

from .gw import get_snr_td

### Save priors
def save_injection_params(fname, injection_params):
    ''' Save injection parameters in prior file ascii format '''
    with open(fname, 'w') as f:
        for key, val in injection_params.items():
            f.write(f'{key} = {val}\n')


### Prior method
def sample_from_prior(prior_file=None, prior_dict=None, source_type='BBH'):    
    ''' Sample from prior. Default prior to bilby.gw.prior.BBHPriorDict()
    
    Parameters:
    - prior_file: path to prior file for bilby to read. default to None
    - prior_dict: bilby piror dictionary. if given, will overwrite prior_file. default to None 
    
    Returns:
    - sample: dict contains parameters sampled from prior
    
    '''
    
    # If both are given
    if prior_file is not None and prior_dict is not None:
        print('WARNING: Both prior_file and prior_dict are given. Use priror_dict.')
    
    # Define prior dict
    # default priors
    if source_type.upper() == 'BBH':
        default_prior = bilby.gw.prior.BBHPriorDict
    elif source_type.upper() == 'BNS':
        default_prior = bilby.gw.prior.BNSPriorDict
    else:
        raise ValueError('source_type must be BBH or BNS.')
        
    if prior_file is None and prior_dict is None:
        priors = default_prior()
    elif prior_dict is not None:
        priors = prior_dict
    elif prior_file is not None:
        priors = default_prior(filename=prior_file)
        
    # Sample and return
    print('Sample from Prior')
    for key, val in priors.items():
        print(f'- {key}: {val}')
    sample = priors.sample()
    
    return sample

### Detector method
def get_detector_response(injection_parameters, waveform_arguments,
                          duration, fs, gps_start, ifo_name, source_type='BBH'):
    ''' Get frequency and time domain detector response for an injected signal 
    
    Parameters:
    - injection_params: injection parameters for waveform generator
    - waveform_params: for bibly.gw.WaveformGenerator
    - duration: duration of injection
    - fs: sampling frequency
    - gps_start: start time of injection
    - ifo_name: name of bilby detector
    - source_type: BBH or BNS
    
    Returns:
    - signal_fd: detector response in frequency domain
    - signal_td: detector response in time domain
    
    '''
    
    # Create the waveform_generator using a LAL BinaryBlackHole source function
    # the generator will convert all the parameters
    if source_type.upper() == 'BBH':
        frequency_domain_source_model = bilby.gw.source.lal_binary_black_hole
        parameter_conversion = convert_to_lal_binary_black_hole_parameters
    elif source_type.upper() == 'BNS':
        frequency_domain_source_model = bilby.gw.source.lal_binary_neutron_star
        parameter_conversion = convert_to_lal_binary_neutron_star_parameters
    else:
        raise ValueError('source_type must be BBH or BNS.')
        
    waveform_generator = bilby.gw.WaveformGenerator(
        duration=duration, sampling_frequency=fs,
        frequency_domain_source_model=frequency_domain_source_model,
        parameter_conversion=parameter_conversion,
        waveform_arguments=waveform_arguments)

    # Set up interferometer and calculate detector response
    ifo = bilby.gw.detector.get_empty_interferometer(ifo_name)
    ifo.set_strain_data_from_power_spectral_density(
        sampling_frequency=fs, duration=duration, start_time=gps_start)

    # Compute detector response in frequency and time domain
    template_fd = waveform_generator.frequency_domain_strain(injection_parameters)
    signal_fd = ifo.get_detector_response(template_fd, injection_parameters)
    signal_td = np.fft.irfft(signal_fd)*fs
    
    return signal_fd, signal_td

def inject_signal(hoft, injection_parameters, waveform_arguments,
                  duration, fs, gps_start, gps_injection_start, 
                  ifo_name, optimal_snr=None, source_type='BBH'):
    ''' Get frequency and time domain detector response for an injected signal 
    
    Parameters:
    - hoft: detector strain to inject GW signal
    - injection_parameters: injection parameters for waveform generator
    - waveform_arguments: for bibly.gw.WaveformGenerator
    - duration: duration of injection
    - fs: sampling frequency
    - optimal_snr: match filtered snr. If given will overwrite luminosity distance
    - gps_start: start time of hoft
    - gps_injection_start: start time of injection
    - ifo_name: name of detector
    
    Return:
    - injected_hoft: detector strain with injected GW
    - signal: dictionary contains signal in frequency and time domain
    '''
    
    injected_hoft = hoft.copy()
    
    # get frequency and time domain detector response
    # print waveform parameters
    print('Waveform parameters: ')
    for key, val in waveform_arguments.items():
        print(f'- {key}: {val}')
    print(f'- Source: {source_type}')
    print('Injection parameters: ')
    for key, val in injection_parameters.items():
        print(f'- {key}: {val}')        
    signal_fd, signal_td = get_detector_response(
        injection_parameters, waveform_arguments, duration, fs, 
        gps_injection_start, ifo_name)
    
    # scale signal and luminosity distance by otpimal SNR if given, else calculate optimal SNR
    freq, psd_hoft = sig.welch(hoft, fs=fs, nperseg=duration*fs)
    if optimal_snr is not None:
        old_snr = float(get_snr_td(psd_hoft, freq, signal_td, fs))
        snr_scale = optimal_snr/old_snr
        signal_td *= float(snr_scale)
        injection_parameters['luminosity_distance'] /= float(snr_scale)
        print(f'- Scale signal from optimal SNR {old_snr} to {optimal_snr}.'\
              'Overwriting luminosity distance.')
        print('- New luminosity distance: %.4f' % 
              injection_parameters['luminosity_distance'])
    else:
        optimal_snr =  float(get_snr_td(psd_hoft, freq, signal_td, fs))
        print(f'- Optimal SNR: {optimal_snr}')
        
    # inject signal into hoft
    start_idx = int((gps_injection_start - gps_start)*fs)
    stop_idx = start_idx + int(duration*fs)
    injected_hoft[:, start_idx:stop_idx] += signal_td
        
    return injected_hoft, {'td': signal_td, 'fd': signal_fd}