
import h5py

import numpy as np
from scipy.interpolate import interp1d

from pycbc.waveform import get_fd_waveform

def get_snr_fd(psd, psd_freq, template, template_freq, fmin=20):
    ''' Compute the SNR of a given gravitational-wave template
    Integrate the frequency from fmin to inf 
    
    Parameters:
    - psd, psd_freq: frequency and power spectral density of noise
    - template, template_freq: GW template strain in frequency domain
    - fmin: minimum frequency 
    Returns:
    - snr: matched filter SNR
    '''

    df = psd_freq[1]-psd_freq[0]
    
    mask = (fmin < psd_freq)  # get frequency of interest
    
    # interpolate template
    abs_template_interp = interp1d(
        template_freq, np.abs(template), bounds_error=False, fill_value=0.)(psd_freq)
    inv_psd = np.zeros_like(psd)
    inv_psd[psd > 0.0] = 1./psd[psd > 0.0]
    
    # SNR^2 = 4 \int_fmin h(f)^2/PSD_n df
    snr = 4.0*np.sum(abs_template_interp[mask]**2*inv_psd[:, mask], 1)*df
    snr = np.sqrt(snr)
        
    return snr

def get_snr_td(psd, psd_freq, template, fs, fmin=20):
    ''' Compute the SNR of a given gravitational-wave template
    Integrate the frequency from fmin to inf 
    
    Parameters:
    - psd, psd_freq: frequency and power spectral density of noise
    - template: GW template strain
    - fs: sampling rate in Hz
    - fmin: minimum frequency 
    
    Returns:
    - snr: matched filter SNR
    '''
        
    # FFT to frequency domain. Normalize by dt=1./fs
    template_fd = np.fft.rfft(template)/fs
    template_freq = np.fft.rfftfreq(len(template))*fs
        
    snr = get_snr_fd(psd, psd_freq, template_fd, template_freq, fmin)
    
    return snr

def get_range_fd(psd, psd_freq, template, template_freq, fmin=20., return_horizon=False):
    ''' Calculate the inspiral range of a given GW template in time-domain
    Horizon distance is defined as the maximum distance to a GW
    strain with SNR = 8
    
    Parameters:
    - psd, psd_freq: frequency and power spectral density of noise
    - template, template_freq: GW template strain in frequency domain
    - fmin: minimum frequency 
    - return_horizon: return horizon distance instead
    
    Returns:
    -  d: average distance or horizon distance of GW template
    '''
    
    # Calculate matched filter SNR
    snr = get_snr_fd(psd, psd_freq, template, template_freq, fmin)
    
    # Calculate horizon and average distance
    d_hor = snr/8
    
    # convert horizon to avg distance
    F_avg = 2.2648  # NOTE: for BNS only
    d_avg = d_hor/F_avg
    
    if return_horizon:
        return d_hor
    return d_avg


def get_range_td(psd, psd_freq, template, fs, fmin=20., return_horizon=False):
    ''' Calculate the inspiral range of a given GW template in time-domain
    Horizon distance is defined as the maximum distance to a GW
    strain with SNR = 8
    
    Parameters:
    - psd, psd_freq: frequency and power spectral density of noise
    - template: GW template strain
    - fs: sampling rate in Hz
    - fmin: minimum frequency 
    - return_horizon: return horizon distance instead
    
    Returns:
    -  d: average distance or horizon distance of GW template
    '''
    
    # Calculate matched filter SNR
    snr = get_snr_td(psd, psd_freq, template, fs, fmin)
    
    # Calculate horizon and average distance
    d_hor = snr/8
    
    # convert horizon to avg distance
    F_avg = 2.2648  # NOTE: for BNS only
    d_avg = d_hor/F_avg
    
    if return_horizon:
        return d_hor
    return d_avg

def get_canonical_range(psd, psd_freq, source='bns', return_horizon=False):
    ''' Get canonical range of a 30-30 Msun BBH or a 1.4-1.4 Msun BNS 
    
    Parameters:
    - psd, psd_freq: frequency and power spectral density of noise
    - source: source type. either BNS or BBH
    - return_horizon: return horizon distance instead
    
    Returns:
    -  d: average distance or horizon canonical distance
    '''

    # choose source type and get template fd
    if source.lower() == 'bbh':
        hp, hc = get_fd_waveform(
            approximant='IMRPhenomPv2',
            mass1=30, mass2=30,
            delta_f=1.0/64,
            distance=1.,
            f_lower=20)    
    elif source.lower() == 'bns':
        hp, hc = get_fd_waveform(
            approximant='TaylorF2',
            mass1=1.4, mass2=1.4,
            delta_f=1.0/64,
            distance=1.,
            f_lower=20)

    # calculate range
    template = np.array(hp)
    template_freq = np.array(hp.sample_frequencies)
    
    d = get_range_fd(psd, psd_freq, template, template_freq, 
                     fmin=20., return_horizon=return_horizon)
    
    return d
