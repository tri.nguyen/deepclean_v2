
import os
import h5py

import scipy.signal as sig

def save_time_series(f, data, dset_name, sample_rate=1., t0=0., channel='', name=None):
    ''' Save time series so it can be read by gwpy.timeseries.TimeSeries '''
    if name is None:
        name = channel
    dset = f.create_dataset(dset_name, data=data, compression='gzip')
    dset.attrs['sample_rate'] = sample_rate
    dset.attrs['t0'] = t0
    dset.attrs['channel'] = channel
    dset.attrs['name'] = name
    return dset
    
    
def save_frequency_series(f, data, dset_name, f0=0.0, df=1., epoch=0., channel='', name=None):
    ''' Save frequency series so it can be read by
    gwpy.frequencyseries.FrequencySeries'''
    if name is None:
        name = channel
    dset = f.create_dataset(dset_name, data=data, compression='gzip')
    dset.attrs['f0'] = f0
    dset.attrs['df'] = df
    dset.attrs['epoch'] = epoch
    dset.attrs['channel'] = channel
    dset.attrs['name'] = name
    return dset
    
def save_training_dataset(
    outdir, label, data, fs=1.0, gps_start=0., chanslist={}, 
    ifo='', injection_params=None, signal=None):
    ''' Save dataset to HDF5 format. Save HOFT and witnesses separately '''
    
    # Create folder if not already existed
    os.makedirs(outdir, exist_ok=True)
    
    if len(chanslist.keys()) == 0:
        raise ValueError('Empty chanslist')
    
    # Save hoft
    hoft = data.get('hoft')
    chans = chanslist.get('hoft', [])
    if hoft is None:
        print('Cannot find HOFT, skipping...')
    else:
        hoft_fname = os.path.join(outdir, f'{label}_hoft.h5')
        with h5py.File(hoft_fname, 'w') as f:
            # save attributes
            f.attrs.update(dict(fs=fs, gps_start=gps_start, ifo=ifo))
            
            # save dataset
            group = f.create_group('hoft')
            group.attrs['chanslist'] = chans 
            save_time_series(group, hoft.ravel(), 'timeseries',
                             sample_rate=fs, t0=gps_start, channel=chans[0])
            
            # save injection parameters if given
            if injection_params is not None:
                group_injection = f.create_group('injection_params')
                group_injection.attrs.update(injection_params)
                group_injection.create_dataset(
                    'signal', data=signal, compression='gzip')
        
    # Save witnesses channel
    if len(chanslist.keys()) == 1 and hoft is not None:
        print('Cannot find witness channels, skipping...')
        return
    
    witnesses_fname = os.path.join(outdir, f'{label}_witnesses.h5')
    with h5py.File(witnesses_fname, 'w') as f:
        # save attributes
        f.attrs.update(dict(fs=fs, gps_start=gps_start, ifo=ifo))
        
        # save witnesses by group name
        for gr, ts in data.items():
            # skip HOFT
            if gr == 'hoft':
                continue
            group = f.create_group(gr)
            group.attrs['chanslist'] = chanslist.get(gr, [])           
            save_time_series(group, ts, 'timeseries', sample_rate=fs,
                             t0=gps_start)

def save_cleaned_dataset(
    outdir, label, data_cleaned, fs=1.0, gps_start=0., chanslist={}, 
    ifo='', data_original=None, injection_params=None, signal=None):
    
    # Create folder if not already existed
    os.makedirs(outdir, exist_ok=True)
    
    fname = os.path.join(outdir, f'{label}_hoft.h5')
    with h5py.File(fname, 'w') as f:
        # save attributes
        f.attrs.update(dict(fs=fs, gps_start=gps_start, ifo=ifo))

        # save dataset
        # cleaned data
        group = f.create_group('hoft')
        group.attrs['chanslist'] = chanslist
        save_time_series(group, data_cleaned.ravel(), 'timeseries',
                         sample_rate=fs, t0=gps_start, channel=chanslist[0])

        # original data
        group_original = f.create_group('hoft_original')
        group_original.attrs['chanslist'] = chanslist
        save_time_series(group_original, data_original.ravel(), 'timeseries',
                         sample_rate=fs, t0=gps_start, channel=chanslist[0])
        
        # save injection parameters if given
        print('injection', injection_params)
        if injection_params is not None:
            group_injection = f.create_group('injection_params')
            group_injection.attrs.update(injection_params)
            if signal is not None:
                group_injection.create_dataset(
                        'signal', data=signal, compression='gzip')
            
        
# def save_pe(fname, hoft, fs=1.0, gps_start=0., gps_injection_start=0., duration=0., 
#             psd_nperseg=256, channel='', injection_params=None, signal=None):
    

#     ''' Save injection dataset '''

#     # Compute and save PSD before and after PE window
#     start_idx = int((gps_injection_start - gps_start)*fs)
#     stop_idx = start_idx + int(duration*fs)
#     _, psd_before = sig.welch(hoft[:, :start_idx], fs=fs, nperseg=psd_nperseg)
#     _, psd_after = sig.welch(hoft[:, stop_idx:], fs=fs, nperseg=psd_nperseg)
#     dfreq = fs/psd_nperseg
    
#     # save a reduced dataset for ease of running PE
#     with h5py.File(fname, 'w') as f:

#         f.attrs['channel'] = channel
        
#         # Save PSD with attributes
#         save_frequency_series(f, psd_before.ravel(), 'psd_before',
#                               df=dfreq, epoch=gps_start, channel=channel)
#         save_frequency_series(f, psd_after.ravel(), 'psd_after',
#                               df=dfreq, epoch=gps_injection_start+duration, channel=channel)
        
#         # Save time series
#         save_time_series(f, hoft[:, start_idx:stop_idx].ravel(), channel,
#                          sample_rate=fs, t0=gps_injection_start, channel=channel)  

#         # Save injected waveform
#         if signal is not None:
#             save_time_series(f, signal.ravel(), 'waveform',
#                              sample_rate=fs, t0=gps_injection_start, channel='WAVEFORM')
            
#         # Save injection parameters
#         if injection_params is not None:
#             group_injection = f.create_group(name='injection_params')
#             group_injection.attrs.update(injection_params)