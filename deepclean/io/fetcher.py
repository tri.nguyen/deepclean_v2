
import sys
import nds2
import configparser

import numpy as np
import scipy.signal as sig

from .config import HANFORD, LIVINGSTON, DEFAULT_NDS_SERVER, DEFAULT_PORT_NUMBER

def read_chanslist(chanslist_f, detector='H1'):
    ''' Read channel lists from config file and append detector prefix '''
    print('Reading channel list from %s with ifo %s.....' % (chanslist_f, detector))
    
    # set detector (no Virgo yet)
    if detector.upper() in HANFORD:
        detector = 'H1:'
    elif detector.upper() in LIVINGSTON:
        detector = 'L1:'
    else:
        raise ValueError('Unknown detector.')
    
    parser = configparser.ConfigParser()
    parser.read(chanslist_f)
    
    groups = parser.sections()
    chanslist = {}
    for gr in groups:
        chanslist[gr] = parser[gr]['channels'].split(', ')
        chanslist[gr] = [detector + c for c in chanslist[gr]]
    return chanslist


def download_data(chanslist, t0, t1):
    ''' Download channels from chanslist in window [t0, t1]'''
    
    print('Downloading data from %d to %d.....' % (t0, t1))
    try:
        conn = nds2.connection(DEFAULT_NDS_SERVER, DEFAULT_PORT_NUMBER)
        conn.set_parameter('ALLOW_DATA_ON_TAPE', 'True')
        conn.set_parameter('GAP_HANDLER', 'STATIC_HANDLER_ZERO')
    except RuntimeError as e:
        print(e)
        print('Have you run `kinit albert.eistein`?')
        sys.exit(1)
        
    # fetch data from server 
    data = {}
    new_chanslist = {}
    for gr, chans in chanslist.items():
        # initialize list
        data[gr] = []
        new_chanslist[gr] = []
        
        print(f'- downloading group: {gr}')
        
        # fetch data from t0 to t1 from the requested channels in group
        try:
            for d in conn.fetch(t0, t1, chans):
                # ignored if the data contains only zero
                if np.any(d.data):
                    chan = d.Channel().Name()
                    fs = d.Channel().SampleRate()
                    data[gr].append((d.data, fs, chan))
                    new_chanslist[gr].append(chan)
                else:
                    print('- ignore %s: all-zeros' % d.Channel().Name())
                    continue
        except:
            # anything can happen
            print(f'- nds2 error, ignore group: {gr}')
    return data, new_chanslist


def fetch_data(chanslist, t0, t1, fs, detector='H1', return_chanslist=False):
    ''' Read channel list, download and resample data '''
    
    # get channel
    if isinstance(chanslist, str):
        chanslist = read_chanslist(chanslist, detector)
    data, chanslist = download_data(chanslist, t0, t1)
    
    # resample data to sample rate 
    resample_data = {}
    for gr, data_list in data.items():
        temp = [] 
        for (d, fs_d, chan) in data_list:
            if fs_d < fs:
                temp.append(sig.resample_poly(d, up=int(fs/fs_d), down=1))
            elif fs_d > fs:
                temp.append(sig.resample_poly(d, up=1, down=int(fs_d/fs)))
            else:
                temp.append(d)
        resample_data[gr] = np.array(temp)
    
    if return_chanslist:
        return resample_data, chanslist
    return resample
