
import torch.optim.lr_scheduler as lr_scheduler

schedulers = {
    'STEPLR': lr_scheduler.StepLR,
    'MULTISTEPLR': lr_scheduler.MultiStepLR,
    'EXPONENTIALLR': lr_scheduler.ExponentialLR,
    'REDUCELRONPLATEAU': lr_scheduler.ReduceLROnPlateau, 
}

def get_scheduler(params, optimizer):
    ''' Get torch optimizer '''
    scheduler = params.pop('scheduler', None)
    if scheduler is None:
        return None
    scheduler = schedulers[scheduler.upper()]
    return scheduler(optimizer, **params)