

import torch
from ..logger import Logger

def print_train():
    ''' Most important function '''
    print('------------------------------------')
    print('------------------------------------')
    print('')
    print('         ..oo0  ...ooOO00           ')
    print('        ..     ...             !!!  ')
    print('       ..     ...      o       \o/  ')
    print('   Y  ..     /III\    /L ---    n   ')
    print('  ||__II_____|\_/| ___/_\__ ___/_\__')
    print('  [[____\_/__|/_\|-|______|-|______|')
    print(' //0 ()() ()() 0   00    00 00    00')
    print('')
    print('------------------------------------')
    print('------------------------------------')
    
def get_device(use_gpu=True):
    ''' Convenient function to set up hardward '''
    if not use_gpu:
        print('Device: CPU')
        return torch.device('cpu')
    if (not torch.cuda.is_available()):
        if use_gpu:
            print('WARNING: GPU is not available. Use CPU instead.')
            print('Device: CPU')
        return torch.device('cpu')
    
    device = torch.device('cuda')
    print('Device: %s' % torch.cuda.get_device_name(device))
    return device
    

def train_on_batch(data, target, model, criterion, optimizer):
    ''' Train on one batch '''
    model.train()
    # Forward pass
    pred = model(data)
    # Backward pass
    loss = criterion(pred, target)
    loss.backward()
    # Gradient descent 
    optimizer.step()
    return loss


def test(data_loader, model, criterion, device='cpu'):
    ''' Test '''
    model.eval()
    loss = 0.
    with torch.no_grad():
        for n_batch, (data, target) in enumerate(data_loader):
            # Move to hardware
            data = data.to(device)
            target = target.to(device)
            # Compute score, loss, and prediction
            pred = model(data)
            if criterion.reduction == 'mean':
                loss += criterion(pred, target)*len(data)
            else:
                loss += criterion(pred, target)
        
        # Normalize loss by the sample size
        loss /= len(data_loader.dataset)
    return loss


def train(train_loader, test_loader, model, criterion, optimizer, 
          scheduler=None, max_epochs=1, logger=None, device='cpu'):
    ''' Train '''
    # Get constants and parameters
    num_batches = len(train_loader)   # total number of batch
    
    if logger is None:
        logger = Logger(outdir='outdir', label='run', metrics=['loss'])
    
    # Start training
    print_train()
    for epoch in range(max_epochs):
        train_loss = 0.
        n_data = 0.
        for n_batch, (data, target) in enumerate(train_loader):
            optimizer.zero_grad()

            # Train on batch
            data = data.to(device)
            target = target.to(device)
            loss = train_on_batch(data, target, model, criterion, optimizer)

            # Update train loss
            if criterion.reduction == 'mean':
                train_loss += loss*len(data)
            else:
                train_loss += loss
            n_data += len(data)

            # Log error and Display progress
            if n_batch % (num_batches//4) == 0:

                train_loss_display = train_loss/n_data

                # Test on test dataset
                test_loss_display = test(test_loader, model, criterion, device)
    
                # updata metric
                logger.update_metric(
                    train_loss_display, test_loss_display, 'loss',
                    epoch, n_batch, num_batches)

                # display status
                logger.display_status(
                    epoch, max_epochs, n_batch, num_batches,
                    train_loss_display, test_loss_display, 'loss'
                )
                
        # update learning rate if scheduler is given
        if scheduler is not None:
            scheduler.step()
                
        # log metric and model every epochs
        logger.log_metric()
        logger.save_models(model, epoch)
        
def predict(data_loader, model, return_numpy=True, device='cpu'):
    ''' Predict given data'''
    model.eval()
    with torch.no_grad():
        prediction = []
        for (data, _) in data_loader:
            prediction.append(model(data.to(device)))
        prediction = torch.cat(prediction)
        if return_numpy:
            return prediction.cpu().numpy()
        return prediction    