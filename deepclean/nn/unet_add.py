import numpy as np

import torch
import torch.nn as nn

from .module import Module

def convkxk(in_filters, out_filters, kernel_size, stride=1):
    ''' Return nn.Conv1d with kernel_size k and same padding'''
    padding = (kernel_size - 1)//2
    return nn.Conv1d(in_filters, out_filters, kernel_size=kernel_size, 
                     stride=stride, padding=padding, bias=False)

def convtranskxk(in_filters, out_filters, kernel_size, stride=1):
    ''' Return nn.ConvTranspose1d with kernel_size k and same padding'''
    padding = (kernel_size - 1)//2
    output_padding = kernel_size % 2
    return nn.ConvTranspose1d(in_filters, out_filters, kernel_size=kernel_size, 
                              stride=stride, padding=padding, output_padding=output_padding,
                              bias=False)

class UNetAdd(Module):
    
    name = 'UNET_ADD'
    
    def __init__(self, input_dim):
        super().__init__()

        # input conv
        self.input_conv = convkxk(input_dim, 8, 15, stride=1)
        self.input_bn = nn.BatchNorm1d(8)
        self.relu = nn.ReLU()
        
        # downsample
        self.conv1 = convkxk(8, 8, 7, stride=2)
        self.bn1 = nn.BatchNorm1d(8)
        self.conv2 = convkxk(8, 16, 7, stride=2)
        self.bn2 = nn.BatchNorm1d(16)
        self.conv3 = convkxk(16, 32, 7, stride=2)
        self.bn3 = nn.BatchNorm1d(32)
        self.conv4 = convkxk(32, 64, 7, stride=2)
        self.bn4 = nn.BatchNorm1d(64)

        # upsample
        self.convtrans1 = convtranskxk(64, 32, 7, stride=2)
        self.bntrans1 = nn.BatchNorm1d(32)
        self.convtrans2 = convtranskxk(32, 16, 7, stride=2)
        self.bntrans2 = nn.BatchNorm1d(16)
        self.convtrans3 = convtranskxk(16, 8, 7, stride=2)
        self.bntrans3 = nn.BatchNorm1d(8)        
        self.convtrans4 = convtranskxk(8, 8, 7, stride=2)
        self.bntrans4 = nn.BatchNorm1d(8)
        
        # output conv
        self.output_conv = convkxk(8, 1, 15, stride=1)
        
    def forward(self, x):
        
        # input
        conv_input = self.relu(self.input_bn(self.input_conv(x)))
        
        # downsample
        conv1 = self.relu(self.bn1(self.conv1(conv_input)))
        conv2 = self.relu(self.bn2(self.conv2(conv1)))
        conv3 = self.relu(self.bn3(self.conv3(conv2)))
        conv4 = self.relu(self.bn4(self.conv4(conv3)))
        
        # upsample
        x = self.convtrans1(conv4)
        x = self.relu(self.bntrans1(x))                
        x += conv3
        x = self.convtrans2(x)
        x = self.relu(self.bntrans2(x))
        x += conv2
        x = self.convtrans3(x)
        x = self.relu(self.bntrans3(x))
        x += conv1
        x = self.convtrans4(x)
        x = self.relu(self.bntrans4(x))
        x += conv_input
        
        # output conv
        x = self.output_conv(x)
        x = x.view(x.shape[0], -1)  # reshape to match dimension for criterion
        return x


    def initialize(self, weight_initializer=None, bias_initializer=None): 
        def init_fn(layer):
            if type(layer) in (nn.Linear, nn.Conv1d, nn.ConvTranspose1d):
                if weight_initializer:
                    weight_initializer(layer.weight)
                if bias_initializer:
                    bias_initializer(layer.bias)
        self.apply(init_fn)