
import torch

from . import utils
from . import autoencoder_simple
from . import autoencoder_groups
from . import unet_cat
from . import unet_add

networks = {
    'AUTOENCODER_SIMPLE': autoencoder_simple.AutoEncoderSimple,
    'AUTOENCODER_GROUPS': autoencoder_groups.AutoEncoderGroups,
    'UNET_CAT': unet_cat.UNetCat,
    'UNET_ADD': unet_add.UNetAdd,
}

def get_network(params, device='cpu'):
    network = params.pop('network')
    net = networks[network.upper()]
    model = net(**params).to(device)
    return model

def load_from_checkpoint(checkpoint_file, params, network=None, device='cpu'):
    if network is not None:
        params['network'] = network
    model = get_network(params, device)
    model.load_state_dict(torch.load(checkpoint_file, map_location=device))
    return model
