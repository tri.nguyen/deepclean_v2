
import numpy as np

import torch
import torch.nn as nn

from .module import Module

class AutoEncoderGroups(Module):
    
    name = 'AUTOENCODER_GROUPS'
    
    def __init__(self, input_dim, filters, kernels, activation='relu', norm_layer=False):
        super().__init__()
        
        if len(filters) != len(kernels):
            raise ValueError('filter length and kernel length must match.')

        self.downsampler = nn.Sequential()
        self.upsampler = nn.Sequential()
        
        # get activation layer    
        activation = activation.upper()    
        act = self._map_activation(activation)
        
        # input layer
        self.input_conv = []
        self.input_conv.append(
            nn.Conv1d(input_dim, input_dim, 7, stride=1, groups=input_dim, padding=3))
        if norm_layer:
            self.input_conv.append(nn.BatchNorm1d(input_dim))
        self.input_conv.append(act)
        self.input_conv = nn.Sequential(*self.input_conv)
        
        # add modules to downsampler
        prev_filter = input_dim
        for i in range(len(filters)):
            # get current kernel and filter
            curr_filter = filters[i]
            curr_kernel = kernels[i]
                        
            # create conv layer and activation layer
            self.downsampler.add_module(
                'CONV_%d' % (i+1), nn.Conv1d(
                prev_filter, curr_filter, curr_kernel, stride=2,
                padding=self._same_padding(curr_kernel)))
            if norm_layer:
                self.downsampler.add_module('BN_%d' % (i+1), nn.BatchNorm1d(curr_filter))
            self.downsampler.add_module('%s_%d' %(activation, i+1), act)
            
            # update filter to next filter
            prev_filter = curr_filter
        
        # add modules to upsampler
        filters = [input_dim, *filters]
        for i in range(len(filters)-1):
            # get current kernel and filter
            curr_ind = len(filters)-1-i
            curr_filter = filters[curr_ind]
            next_filter = filters[curr_ind-1]
            curr_kernel = kernels[curr_ind-1]
                            
            # create conv layer and activation layer
            self.upsampler.add_module(
                'CONVTRANS_%d' % (i+1), nn.ConvTranspose1d(
                    curr_filter, next_filter, curr_kernel, stride=2,
                    padding=self._same_padding(curr_kernel), 
                    output_padding=(curr_kernel%2)))
            if norm_layer:
                self.upsampler.add_module('BN_%d' % (i+1), nn.BatchNorm1d(next_filter))
            self.upsampler.add_module('%s_%d' % (activation, i+1), act)
        
        # output layer
        self.output_conv = nn.Conv1d(filters[0], 1, 7, stride=1, padding=3)

    def forward(self, x):
        x = self.input_conv(x)
        x = self.upsampler(self.downsampler(x))
        x = self.output_conv(x)
        x = x.view(x.shape[0], -1)
        return x

    def initialize(self, weight_initializer=None, bias_initializer=None): 
        def init_fn(layer):
            if type(layer) in (nn.Linear, nn.Conv1d, nn.ConvTranspose1d):
                if weight_initializer:
                    weight_initializer(layer.weight)
                if bias_initializer:
                    bias_initializer(layer.bias)
        self.apply(init_fn)
    
    def _same_padding(self, kernel_size):
        ''' Calculate same padding for conv and conv tranpose layer. '''
        return (kernel_size-1)//2
    