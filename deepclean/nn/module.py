
import os
import numpy as np

import torch
import torch.nn as nn
from torch.utils.data import DataLoader

# custom activation function
class LeeActivation(nn.Module):
    
    def __init__(self):
        super().__init__()
        
    def forward(self, x):
        return x/torch.sqrt(1.+x**2)
    
class Identity(nn.Module):
    
    def __init__(self):
        super().__init__()
        
    def forward(self, x):
        return x

# Module class
class Module(nn.Module):
    
    def __init__(self):
        super().__init__()
        
    def _map_activation(self, activation, *args):
        ''' Map activation str to nn.Module '''
        # check type
        if isinstance(activation, nn.Module):
            # already nn.Module
            return activation 
        elif not isinstance(activation, str):
            raise TypeError('activation must be of type str.')
        
        act_dict = {
            'TANH': nn.Tanh,
            'RELU': nn.ReLU,
            'SIGMOID': nn.Sigmoid,
            'LEE_ACTIVATION': LeeActivation,
            'LINEAR': Identity,
        }

        return act_dict.get(activation)(*args)
    
    @property
    def num_params(self):
        ''' Number of parameters in network '''
        return np.sum([np.prod(p.size()) for p in self.parameters()])
