
import torch
import torch.nn as nn

class MSELoss(nn.Module):
    ''' MSE loss '''
    def __init__(self, reduction='mean', eps=1e-8):
        super().__init__()
        self.reduction = reduction
        self.eps = eps
    
    def forward(self, pred, target):
        loss = (target - pred) ** 2
#         loss = torch.abs(target - pred) / (torch.abs(target) + self.eps)
        loss = torch.mean(loss, 1)
        
        # Averaging over patch
        if self.reduction == 'mean':
            loss = torch.sum(loss)/len(pred)
        elif self.reduction == 'sum':
            loss = torch.sum(loss)
            
        return loss