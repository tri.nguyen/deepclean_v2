
import torch.nn as nn

from .mse import MSELoss
from .psd import PSDLoss

class CompositePSDLoss(nn.Module):
    ''' PSD + MSE Loss with weight '''
    
    def __init__(self, fs=1.0, fl=20., fh=500., nperseg=256, noverlap=None, 
                 unit='sec', asd=False, average='mean', reduction='mean',
                 psd_weight=0.5, mse_weight=0.5, device='cpu'):
        super().__init__()
        self.reduction = reduction
        
        self.psd_loss = PSDLoss(
            fs=fs, fl=fl, fh=fh, nperseg=nperseg, noverlap=noverlap, unit=unit, 
            asd=asd, average=average, reduction=reduction, device=device)
        self.mse_loss = MSELoss(reduction=reduction)
        
        self.psd_weight = psd_weight
        self.mse_weight = mse_weight
                
    def forward(self, pred, target):
        # if weight is 0: only run 1 to save computational time
        if self.psd_weight == 0:
            return self.mse_loss(pred, target)
        if self.mse_weight == 0:
            return self.psd_loss(pred, target)
        
        psd_loss = self.psd_weight * self.psd_loss(pred, target)
        mse_loss = self.mse_weight * self.mse_loss(pred, target)
        
        return (psd_loss + mse_loss) / (self.psd_weight + self.mse_weight)