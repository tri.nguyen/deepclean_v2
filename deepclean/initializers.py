
from torch.nn import init

INITIALIZERS = {
    'UNIFORM': init.uniform_,
    'NORMAL': init.normal_,
    'CONSTANT': init.constant_,
    'EYE': init.eye_,
    'DIRAC': init.dirac_,
    'XAVIER_UNIFORM': init.xavier_uniform_,
    'XAVIER_NORMAL': init.xavier_normal_,
    'KAIMING_UNIFORM': init.kaiming_uniform_,
    'KAIMING_NORMAL':init.kaiming_normal_,
    'ORTHOGONAL': init.orthogonal_,
    'SPARSE': init.sparse_,
}

def avail():
    return iter(INITIALIZERS.keys())

def parse(initializer):
    ''' Map initializer str to init function '''
    if not isinstance(initializer, str):
        raise TypeError('initializer must be of type str.')
    return INITIALIZERS[initial-izer.upper()]
    
def get(initializer=None, **defaults):
    if initializer:
        return lambda tensor: parse(initializer)(tensor, **defaults)
    return