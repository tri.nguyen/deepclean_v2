
import os
import h5py

import copy
import numpy as np
import scipy.signal as sig

import torch

from . import utils
from ..io import fetcher
from ..io import utils as io_utils


def get_timeseries(indir_hoft, indir_witnesses, params, aux_groups):
    ''' Get timeseries based on parameters '''
    # Get params
    series_type = params['series_type']
    train_kernel = params['train_kernel']
    train_stride = params['train_stride']
    target_kernel = params['target_kernel']
    target_stride = params['target_stride']
    unit = params['unit']
    pad_mode = params['pad_mode']
    preprocess = params['preprocess']
    dataset_fname = io_utils.get_dataset_filename(indir_hoft, indir_witnesses)
        
    # initialize timeseries
    if series_type == 'TimeSeries':
        train_ts = TimeSeries(dataset_fname['training'], aux_groups)
        target_ts = TimeSeries(dataset_fname['target'], aux_groups)
    elif series_type == 'TimeSeriesCNN':
        train_ts = TimeSeriesCNN(
            train_kernel, train_stride, unit=unit, pad_mode=pad_mode,
            filename=dataset_fname['training'], aux_groups=aux_groups)
        target_ts = TimeSeriesCNN(
            target_kernel, target_stride, unit=unit, pad_mode=pad_mode,
            filename=dataset_fname['target'], aux_groups=aux_groups)       
    else:
        raise ValueError('series_type must be "TimeSeries" or "TimeSeriesCNN".')
    
    # preprocess timeseries
    if preprocess:
        train_ts, target_ts = utils.preprocess_training(
            train_ts, target_ts, params) 
           
    return train_ts, target_ts
    
class TimeSeries:
    ''' Timeseries torch dataset'''

    def __init__(self, filename=None, aux_groups=None):
        ''' Initialize attributes '''
        self.hoft = None
        self.witnesses = None
        self.t_start = None
        self.fs = None
        self.ifo = None
        self.channels = None
        self.scale_constants = [1., 1.]
        self.shift_constants = [0., 0.]
        self.injection_params = None
        self.signal = None
        if filename is not None:
            self.load_data(filename, aux_groups)

    def load_data(self, filename, aux_groups):
        ''' Load dataset from HDF5 file '''
        
        hoft_fname = filename['hoft']
        witnesses_fname = filename['witnesses']
            
        self.channels = {}
        with h5py.File(hoft_fname, 'r') as f:
            # load dataset attributes
            self.t_start = f.attrs['gps_start']
            self.fs = f.attrs['fs']
            self.ifo = f.attrs['ifo']
            
            # import HOFT
            self.hoft = f['hoft']['timeseries'][:].reshape(1, -1)
            self.channels['hoft'] = list(f['hoft'].attrs['chanslist'])
            
            # load injection parameters
            if f.get('injection_params') is not None:
                self.injection_params = dict(f['injection_params'].attrs)
                # TODO: Find a way to check if a HDF5 Dataset exists
                try:
                    self.signal = f['injection_params'][:]
                except:
                    self.signal = None
            
        with h5py.File(witnesses_fname, 'r') as f:
            # load dataset attributes
            self.t_start = f.attrs['gps_start']
            self.fs = f.attrs['fs']
            self.ifo = f.attrs['ifo']
            
            # import aux channels by group
            self.witnesses = []
            for gr in aux_groups:
                if f.get(gr) is None:
                    raise KeyError(f'{gr} witness channel group not found')
                
                self.witnesses.append(f[gr]['timeseries'][:])
                self.channels[gr] = list(f[gr].attrs['chanslist'])
            self.witnesses = np.concatenate(self.witnesses)

        # Reset scale and shift
        self.scale_constants = [1., 1.]
        self.shift_constants = [0., 0.]
            
    def fetch_data(self, chanslist, t_start, duration, ifo, fs):
        ''' Fetch data using NDS2 '''
        data, chanslist = fetcher.fetch_data(
            chanslist=chanslist,
            t0=t_start,
            t1=t_start+duration,
            detector=ifo,
            fs=fs,
            return_chanslist=True
        )
        
        # load data attributes
        self.t_start = t_start
        self.ifo = ifo
        self.fs = fs
        self.channels = chanslist
        self.scale_constants = [1., 1.]
        self.shift_constants = [0., 0.]
        self.injection_params = None
        
        # load channels
        self.hoft = data['hoft']
        self.witnesses = []
        
        for gr, d in data.items():
            if gr == 'hoft':
                continue
            self.witnesses.append(d)
        self.witnesses = np.concatenate(self.witnesses)
        
    def scale(self, mode='constant', constant_values=None):
        ''' Scale data '''
        if mode == 'std':
            # Scale by standard deviation
            scale_hoft, scale_witnesses = self.std
        elif mode == 'constant':
            # Scale by a constant
            scale_hoft, scale_witnesses = constant_values
        else:
            raise ValueError('invalid mode')
        
        # Scale timeseries
        self.hoft /= scale_hoft
        self.witnesses /= scale_witnesses
        
        # Updata scale 
        self.scale_constants[0] /= scale_hoft
        self.scale_constants[1] /= scale_witnesses        
            
    def unscale(self):
        ''' Unscale data '''
        self.hoft /= self.scale_constants[0]
        self.witnesses /=  self.scale_constants[1]
        self.scale_constants = [1., 1.]

    def shift(self, mode='constant', constant_values=None):
        ''' Shift data'''
        if mode == 'mean':
            # Shift by mean
            shift_hoft, shift_witnesses = self.mean
        elif mode == 'constant':
            # Shift by a constant
            shift_hoft, shift_witnesses = constant_values
        else:
            raise ValueError('invalid mode')
        
        # Shift timeseries
        self.hoft = self.hoft - shift_hoft
        self.witnesses = self.witnesses - shift_witnesses
        
        # Updata shift consta
        self.shift_constants[0] -= shift_hoft
        self.shift_constants[1] -= shift_witnesses   
    
    def unshift(self):
        ''' Unshift data '''
        self.hoft -= self.shift_constants[0]
        self.witnesses -=  self.shift_constants[1]
        self.shift_constants = [0., 0.]        
        
    def bandpass_filter(self, fl, fh, order=8, which='hoft'):
        ''' Apply a bandpass filter to a given channel '''
        
        # Choose channel and apply filter        
        target = np.concatenate([self.hoft, self.witnesses])
        target_filt = utils.bandpass_filter(target, self.fs, fl, fh, order, axis=-1)
        hoft_filt, witnesses_filt = target_filt[:1], target_filt[1:]
        
        if which == 'witnesses':
            self.witnesses = witnesses_filt
        elif which == 'hoft':
            self.hoft = hoft_filt
        elif which == 'both':
            self.hoft = hoft_filt
            self.witnesses = witnesses_filt
        
    @property
    def n_channels(self):
        ''' Get number of channels '''
        n = 0
        for gr, val in self.channels.items():
            n += len(val)
        return n
        
    @property
    def mean(self):
        ''' Get mean of hoft and witnesses '''
        mean_hoft = self.hoft.mean(1, keepdims=True)
        mean_witnesses = self.witnesses.mean(1, keepdims=True)
        return mean_hoft, mean_witnesses

    @property
    def std(self):
        ''' Get std of hoft and witnesses '''
        std_hoft = self.hoft.std(1, keepdims=True)
        std_witnesses = self.witnesses.std(1, keepdims=True)
        return std_hoft, std_witnesses

class TimeSeriesCNN(TimeSeries):
    ''' LIGO Timeseries class for CNN '''

    def __init__(self, kernel, stride, unit='sec', pad_mode='median', filename=None,
                 aux_groups=None):
        super().__init__(filename, aux_groups)
        self.kernel = kernel
        self.stride = stride
        self.pad_mode = pad_mode
        
        # unit conversion
        if unit == 'sec':
            self.kernel = int(self.kernel*self.fs)
            self.stride = int(self.stride*self.fs)
        elif unit == '':
            self.kernel = int(self.kernel)
            self.stride = int(self.stride)
        else:
            raise ValueError('unit must be "sec" or "step"')
        
        if self.__len__() == 0:
            raise AttributeError('Invalid kernel and size: len = 0.')

    def __len__(self):
        ''' Return number of stride '''
        length = self.hoft.shape[-1]
        n_stride = int(np.ceil((length-self.kernel)/self.stride) + 1)
        return max(0, n_stride)

    def __getitem__(self, idx):
        ''' Get index '''
        # Check if idx is valid:
        if idx < 0:
            idx +=  self.__len__()
        if idx >= self.__len__():
            raise IndexError(
                f'index {idx} is out of bound with size {self.__len__()}.')

        # Get lookback timeseries
        hoft = self.hoft[:, idx*self.stride:idx*self.stride+self.kernel].copy()
        witnesses = self.witnesses[:, idx*self.stride:idx*self.stride+self.kernel].copy()
        length = hoft.shape[-1]
        
        # Pad median if necessary
        if length < self.kernel:
            pad = self.kernel - length
            hoft = np.pad(hoft, ((0, 0), (0, pad)), mode=self.pad_mode)
            witnesses = np.pad(witnesses, ((0, 0), (0, pad)), mode=self.pad_mode)
            
        # Convert to Tensor
        witnesses = torch.Tensor(witnesses)
        hoft = torch.Tensor(hoft).squeeze()
            
        return witnesses, hoft


# class TimeSeriesRNN(TimeSeries):
#     ''' LIGO look back time series for RNN '''
    
#     def __init__(self, kernel, lookback, filename=None):
#         super().__init__(filename)
        
#         self.kernel= kernel
#         self.lookback = lookback
#         self.x_lookback = None
#         if series is not None:
#             # type check
#             if not isinstance(series, BaseSeries):
#                 raise TypeError('series must be of type deepclean.data.Series.')
#             self.data = series.data
#             self.fs = series.fs
#             self.t0 = series.t0
#             self.t1 = series.t1
#             self.chanslist = series.chanslist
#             self._lookback()

#     def __len__(self):
#         return max(0, int(np.ceil(len(self.data)/self.L1)-1)) + 1

#     def __getitem__(self, idx):

#         # check if data exists
#         if self.data is None: raise AttributeError('data is not initialized')
#         if self.__len__() == 0: raise AttributeError('len = 0')

#         # check if idx is valid
#         if idx < 0: idx += self.__len__()
#         if idx >= self.__len__():
#             raise IndexError(f'index {idx} is out of bound with size {self.__len__()}.')

#         # get index
#         x = self.x_lookback[idx*self.L1:(idx+1)*self.L1]
#         y = self.y[idx*self.L1:(idx+1)*self.L1]
#         y = np.squeeze(y)

#         # convert to tensor and return x, y
#         x, y = torch.Tensor(x), torch.Tensor(y)
#         return x, y

#     def _lookback(self):
#         x = np.pad(self.x, ((self.L2-1, 0), (0, 0)), mode='mean')
#         self.x_lookback = utils.lookback(x, self.L2, 1)

#     def fetch_data(self, *args, **kargs):
#         super().load_data(*args, **kargs)
#         self._lookback()

#     def load_data(self, *args, **kargs):
#         super().load_data(*args, **kargs)
#         self._lookback()

#     def normalize(self, *args, **kargs):
#         super().normalize(*args, **kargs)
#         self._lookback()

#     def denormalize(self, *args, **kargs):
#         super().denormalize(*args, **kargs)
#         self._lookback()

#     def phase_filter(self, fs, fl, fh, which='y'):
#         super().phase_filter(fs, fl, fh, which)
#         if which == 'x': self._lookback()