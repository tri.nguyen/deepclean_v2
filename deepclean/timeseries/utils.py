
import numpy as np
import scipy.signal as sig
from scipy.interpolate import interp1d


### Convienence functions
def preprocess_training(train_ts, target_ts, params):
    ''' Preprocess data for training '''
    
    # Get parameters
    norm = params['norm']
    filt = params['filt']
    filt_order = params['filt_order']
    filt_which = params['filt_which']
    filt_fl = params.get('preprocess_fl')
    filt_fh = params.get('preprocess_fh')
    if filt_fl is None: filt_fl = params['filt_fl']
    if filt_fh is None: filt_fh = params['filt_fh']        
    
    # Bandpass filter
    if filt:
        target_ts.bandpass_filter(filt_fl, filt_fh, filt_order, filt_which)
        train_ts.bandpass_filter(filt_fl, filt_fh, filt_order, filt_which)

    # Normalize
    if norm:
        target_ts.shift('constant', train_ts.mean)
        target_ts.scale('constant', train_ts.std)
        train_ts.shift('mean')
        train_ts.scale('std')
        
    return train_ts, target_ts
        
def preprocess_cleaning(ts, params):
    ''' Preprocess target dataset for cleaning'''
    
    # Get parameters
    norm = params['norm']
    filt = params['filt']
    shift = params['shift']
    scale = params['scale']
    filt_order = params['filt_order']
    filt_which = params['filt_which']
    filt_fl = params.get('preprocess_fl')
    filt_fh = params.get('preprocess_fh')
    if filt_fl is None: filt_fl = params['filt_fl']
    if filt_fh is None: filt_fh = params['filt_fh']        
    
    # Preprocess
    # bandpass-filter
    if filt:
        ts.bandpass_filter(filt_fl, filt_fh, filt_order, filt_which)
    
    # normalize
    if norm:
        shift = [-s for s in shift]
        scale = [1./s for s in scale]
        ts.shift('constant', shift)
        ts.scale('constant', scale)
    
    return ts

def postprocess(prediction_batches, fs, noverlap, window_type, params):
    ''' Postprocess prediction batches to prediction timeseries '''
    
    # Get parameters
    norm = params['norm']
    filt = params['filt']
    shift = params['shift']
    scale = params['scale']
    filt_order = params['filt_order']
    filt_which = params['filt_which']
    filt_fl = params['postprocess_fl']
    filt_fh = params['postprocess_fh']
    
    # overlap-add
    prediction = overlap_add(prediction_batches, noverlap, window_type)    
    
    # rescale and reshift
    if norm:
        prediction /= scale[0] # rescale
        prediction -= shift[0] # reshift

    # filter-add
    prediction = filter_add(prediction, fs, flow=filt_fl, fhigh=filt_fh,
                            order=filt_order, axis=-1)
    
    return prediction

def get_window(nperseg, noverlap, window_type='boxcar'):
    ''' Get normalized windowing function for overlap-add method'''
    
    # rectangular window = box car
    if window_type == 'rectangular':
        window_type = 'boxcar'
    window_fn = vars(sig)[window_type]

    # Get window and normalize
    window = window_fn(nperseg)*(nperseg-noverlap)/nperseg
    if window_type != 'boxcar':
        window *= 2
    return window

### Signal processing functions
def bandpass_filter(data, fs, fl, fh, order=8, axis=-1):
    ''' Apply Butterworth bandpass filter
    using scipy.signal.sosfiltfilt method
    
    Parameters:
    - data: array
    - fs: sampling frequency
    - fl, fh: low and high frequency for bandpass
    - axis: axis to apply the filter on 
    
    Returns:
    - data_filt: filtered array 
    
    '''
    # Make filter
    nyq = fs/2.
    low, high = fl/nyq, fh/nyq  # normalize frequency
    z, p, k = sig.butter(order, [low, high], btype='bandpass', output='zpk')
    sos = sig.zpk2sos(z, p, k)

    # Apply filter and return output
    data_filt = sig.sosfiltfilt(sos, data, axis=axis)
    return data_filt

def overlap_add(data, noverlap, window_type, verbose=True):
    '''Overlap-add method 
    
    Parameters:
    - data: array of shape (N, nperseg)
    - noverlap: number of overlapping samples
    - window_type: window to apply to each segment
    - verbose: if true, print 
    
    Returns:
    - data_add
    
    '''
    N, nperseg = data.shape
    stride = nperseg - noverlap
    
    if verbose:
        print(f'- Length - {nperseg} - Stride - {stride} - Overlap - {noverlap}')
        print(f'- Window: {window_type}')
    
    # Get window function and reshape to match expected dimension
    window = get_window(nperseg, noverlap, window_type)
    window = window.reshape(1, -1)
    
    # overlap-add
    data_add = np.zeros((1, (N-1)*stride+nperseg))
    for i, data_batch in enumerate(data):
        data_add[:, i*stride: i*stride+nperseg] += data_batch*window
    
    return data_add

def filter_add(data, fs, flow, fhigh, order=8, axis=-1, verbose=True):
    ''' Add data with sub bandpass filter
    
    Parameters:
    - data
    - fs: sampling frequency
    - flow, fhigh: list of low and high frequencies
    - verbose: if true, print
    
    Returns:
    - data_add
    
    '''
    if verbose:
        print(f'Apply Butterworth bandpass of order {order}')
    print('- Low frequency: {}'.format(flow))
    print('- High frequency: {}'.format(fhigh))
    
    if isinstance(flow, (int, float)):
        flow = (flow, )
    if isinstance(fhigh, (int, float)):
        fhigh = (fhigh, )
                
    # start adding
    data_add = np.zeros_like(data)
    for fl, fh in zip(flow, fhigh):
        data_add += bandpass_filter(data, fs, fl, fh, order=order, axis=axis)
    
    return data_add
    
def whiten(data, psd, psd_freq, fs):
    ''' Whiten a time series by a given noise PSD
    
    Parameters:
    - data: array of shape (N, L)
    - psd, psd_freq: frequency and psd of the noise
    - fs: sampling rate in Hz
    
    Returns:
    - whiten data
    '''
    
    N, L = data.shape
    
    # zero padding 
    pad = L//4
    data_pad = np.pad(data, ((0, 0), (pad, pad)), mode='constant', constant_values=0)
    
    # Calculate the FFT of data
    data_fd = np.fft.rfft(data_pad)
    data_freq = np.fft.rfftfreq(data_pad.shape[1])*fs
    
    # interpolate noise spectrum and calculate the inverse
    psd_interp = interp1d(psd_freq, psd)(data_freq)
    inv_psd = np.zeros_like(psd_interp)
    inv_psd[psd_interp > 0.0] = 1./psd_interp[psd_interp > 0.0]    

    # Whiten data by divide the FFT by noise ASD
    # ASD = sqrt(PSD)
    data_fd_whiten = data_fd * np.sqrt(2*inv_psd/fs)
    
    # remove zero-frequency (DC) component
    data_fd_whiten[:, 0] = 0.
    
    # Inverse FFT to get time domain and take care of padding
    data_whiten = np.fft.irfft(data_fd_whiten)
    data_whiten = data_whiten[:, pad:-pad]

    return data_whiten

    

# def lookback(data, length, stride=1):
#     # get input dimension
#     if data.ndim < 2: 
#         x = data.copy().reshape(-1, 1)
#     else:
#         x = data.copy()
        
#     n, d = x.shape
#     n_seq = int(np.ceil((n-length)/stride) + 1)

#     # lookback
#     out = np.zeros((n_seq, d, length))
#     for i in range(n_seq):
#         temp = x[i*stride:i*stride+length]
#         try:
#             out[i] = temp.T
#         except ValueError:
#             # pad zero if length is less
#             p = length - len(temp)
#             temp = np.pad(temp, ((0, p), (0, 0)),
#                 mode='constant', constant_values=0)
#     return out