#!/usr/bin/env python

import os
import argparse
import subprocess


def parse_cmd():
    
    parser = argparse.ArgumentParser(
        prog=os.path.basename(__file__), usage='%(prog)s [options]')
        
    # Add required arguments
    parser.add_argument(
        'config', help='path to setup config file',
        type=str, metavar='<path>')
    
    # Add optional arguments
    # general arguments
    parser.add_argument(
        '--skip-setup', help='skip dc-setup',
        action='store_true')
    parser.add_argument(
        '--datadir', help="path to dataset directory", 
        dest="datadir", type=str, metavar='<path>')
    parser.add_argument(
        '--datadir-hoft', help="path to HOFT dataset directory", 
        dest="datadir_hoft", type=str, metavar='<path>')
    parser.add_argument(
        '--datadir-witnesses', help="path to witnesses dataset directory", 
        dest="datadir_witnesses", type=str, metavar='<path>')
    parser.add_argument(
        '--outdir', help="path to output directory", 
        dest="outdir", type=str, metavar='<path>')
    
    # dc-setup arguments
    parser.add_argument(
        '--gps-start', help='gps start time of targeted frame',
        type=int, metavar='<sec>')
    parser.add_argument(
        '--duration', help='duration of targeted frame',
        type=int, metavar='<sec>')
    parser.add_argument(
        '--gps-train-start', help='gps start time of training frame',
        type=int, metavar='<sec>')
    parser.add_argument(
        '--train-duration', help='duration of training frame',
        dest='train_duration', type=int, metavar='<sec>')
    parser.add_argument(
        '--chanslist', help='channel file to read',
        dest='chanslist', type=str, metavar='<path>')    
    parser.add_argument(
        '--sampling-frequency', help='sampling frequency',
        type=int, metavar='<Hz>')
    parser.add_argument(
        '--ifo', help='interferometer', 
        type=str, metavar='<interferometer>')
    # dc-train arguments
    parser.add_argument(
        '--aux-groups', help="aux groups",
        dest="aux_groups", nargs='+', type=str, metavar='<str>')
    parser.add_argument(
        '--batch-size', help='batch size',
        type=int, metavar='<int>')
    parser.add_argument(
        '--max-epochs', help='max epochs',
        type=int, metavar='<int>')
    # dc-clean arguments
    parser.add_argument(
        '--save-original', help='save the original HOFT for comparision',
        type=bool, metavar='<bool>')
    parser.add_argument(
        '--use-gpu', help='run on gpu',
        type=bool, metavar='<bool>')
    
    # Parse as dictionary
    params = vars(parser.parse_args())
    datadir = params.pop('datadir')
    datadir_hoft = params.get('datadir_hoft')
    datadir_witnesses = params.get('datadir_witnesses')    
    if datadir is not None:
        if (datadir_hoft is not None) or (datadir_witnesses is not None):
            raise ValueError('"datadir_hoft" or "datadir_witnesses" cannot be given'\
                             ' if "datadir" is given')
        params['datadir_hoft'] = datadir
        params['datadir_witnesses'] = datadir          
    return params
    
params = parse_cmd()

# create command
def create_append(params, parse_dict):
    append = ''
    if isinstance(parse_dict, dict):
        for key, val in arguments.items():
            if params.get(val) is not None:
                append += f' --{key} {params[val]}'
    elif isinstance(parse_dict, (list, tuple)):
        for key in parse_dict:
            val = key.replace('-', '_')            
            if params.get(val) is not None:
                if key == 'aux-groups':
                    aux_groups = ''
                    for gr in params[val]:
                        aux_groups += gr
                        aux_groups += ' '
                    aux_groups = aux_groups[:-1] # exclude last white space
                    params[val] = aux_groups
                append += f' --{key} {params[val]}'
    return append
general_append = create_append(params, parse_dict=('outdir'))
setup_append = create_append(params, parse_dict=(
        'gps-start', 'duration', 'gps-train-start', 'train-duration',
        'chanslist', 'sampling-frequency', 'ifo', 'datadir')    
)
train_append = create_append(
    params, parse_dict=('aux-groups', 'batch-size', 'max-epochs', 'use-gpu',
                        'datadir-hoft', 'datadir-witnesses')
)
clean_append = create_append(params, parse_dict=(
    'save-original', 'use-gpu', 'datadir-hoft', 'datadir-witnesses'))

config = params['config']
setup_cmd = f'dc-setup {config}' + general_append + setup_append
train_cmd = f'dc-train {config}' + general_append + train_append
clean_cmd = f'dc-clean {config}' + general_append + clean_append

# run commaned
if not params['skip_setup']:
    print('Call dc-setup.....')
    print('Command: ' + setup_cmd)
    subprocess.check_call(setup_cmd.split(' '))
    print('-----------------')
print('Call dc-train.....')
print('Command: ' + train_cmd)
subprocess.check_call(train_cmd.split(' '))
print('-----------------')
print('Call dc-clean.....')
print('Command: ' + clean_cmd)
subprocess.check_call(clean_cmd.split(' '))
