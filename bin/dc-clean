#!/usr/bin/env python

''' Predict timeseries given the model checkpoint and the input data 
    Cleaning using overlap-add method '''

from __future__ import absolute_import, print_function

import os
import argparse
import pickle
import time

import numpy as np
import torch
from torch.utils.data import DataLoader

import deepclean as dc

torch.set_default_tensor_type(torch.FloatTensor)

t_start = time.time() # keep track of wall time

# parse cmd argument
def parse_cmd():
    parser = argparse.ArgumentParser(
        prog=os.path.basename(__file__), usage='%(prog)s [options]')         
    
    # Add required argument
    parser.add_argument(
        'config', help='path to setup config file',
        type=str, metavar='<path>')

    # Add optional argument    
    parser.add_argument(
        '--datadir', help="path to dataset directory", 
        dest="datadir", type=str, metavar='<path>')
    parser.add_argument(
        '--datadir-hoft', help="path to HOFT directory", 
        dest="datadir_hoft", type=str, metavar='<path>')
    parser.add_argument(
        '--datadir-witnesses', help="path to witnesses directory",
        dest="datadir_witnesses", type=str, metavar='<path>')
    parser.add_argument(
        '--outdir', help="path to output directory", 
        dest="outdir", type=str, metavar='<path>')
    parser.add_argument(
        "--checkpoint", help="path to model checkpoint to use",
        type=str, metavar='<path>')
    parser.add_argument(
        '--save-original', help='save the original HOFT for comparision',
        type=bool, metavar='<bool>')
    parser.add_argument(
        '--skip-training', help='skip training dataset',
        type=bool, metavar='<bool>')
    parser.add_argument(
        '--use-gpu', help='run on gpu',
        type=bool, metavar='<bool>')
    
    params = vars(parser.parse_args())
    
    datadir = params.pop('datadir')
    datadir_hoft = params.get('datadir_hoft')
    datadir_witnesses = params.get('datadir_witnesses')
    if datadir is not None:
        if (datadir_hoft is not None) or (datadir_witnesses is not None):
            raise ValueError('"datadir_hoft" or "datadir_witnesses" cannot be given'\
                             ' if "datadir" is given')
        params['datadir_hoft'] = datadir
        params['datadir_witnesses'] = datadir
    return params

cmd_params = parse_cmd()

# Parse configuarion file and get parameters
cleaning_params = dc.io.parser.parse_section(cmd_params['config'], 'cleaning')
dc.io.utils.replace_keys(cleaning_params, cmd_params)
outdir = cleaning_params['outdir']

setting_file = os.path.join(outdir, f'setting.bin')
with open(setting_file, 'rb') as f:
    setting_params = pickle.load(f)
    network_params = setting_params.pop('network_params')

# Setup hardware setting
device = dc.nn.utils.get_device(cleaning_params['use_gpu'])

# Get checkpoint file and load model
default_ckpt = dc.io.utils.get_default_checkpoint_file(outdir)
ckpt = cleaning_params.get('checkpoint', default_ckpt)
print(f'Loading model checkpoint from {ckpt}....')
model = dc.nn.load_from_checkpoint(
    ckpt, network_params, network=setting_params.get('network'), device=device)

# Loop over dataset in input directory
dataset_fname = dc.io.utils.get_dataset_filename(
    cleaning_params['datadir_hoft'], cleaning_params['datadir_witnesses'])

for label, fname in dataset_fname.items():
    if label == 'training' and cleaning_params['skip_training']:
        continue
    print('-----------------------------------')
    print(f'Run label: {label}')
        
    # Load dataset into dataloader and preprocess
    print('Loading dataset and preprocessing....')
    ts = dc.timeseries.TimeSeriesCNN(
        cleaning_params['kernel'], cleaning_params['stride'], 
        unit=cleaning_params['unit'], pad_mode=cleaning_params['pad_mode'], 
        aux_groups=setting_params['aux_groups'], filename=fname)
    hoft = ts.hoft.copy()
    witnesses = ts.witnesses.copy()
    ts = dc.timeseries.utils.preprocess_cleaning(ts, setting_params)
    data_loader = DataLoader(ts, batch_size=32, shuffle=False)

    # Predict noise contribution from witness channels and subtract 
    print('Predicting noise coupling......')
    t_predict_start = time.time() # keep track of predicting time
    hoft_pred_batches = dc.nn.utils.predict(data_loader, model, device=device)
    hoft_pred = dc.timeseries.utils.postprocess(
        hoft_pred_batches, ts.fs, ts.kernel-ts.stride, 
        cleaning_params['window_type'], setting_params)
    hoft_pred = hoft_pred[:, :hoft.size]
    hoft_cleaned = hoft - hoft_pred
    predict_time = time.time() - t_predict_start

    # Save output
    hoft = hoft if cleaning_params['save_original'] else None
    dc.io.hdf5.save_cleaned_dataset(
        outdir, label, hoft_cleaned, fs=ts.fs, gps_start=ts.t_start, 
        chanslist=ts.channels.get('hoft'), ifo=ts.ifo, data_original=hoft, 
        injection_params=ts.injection_params, signal=ts.signal
    )

    # Print wall time
    print('Predicting time - %.4e secs' % predict_time)
    
run_time = time.time() - t_start
print('Wall time - %.4e secs' % run_time)
