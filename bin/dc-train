''' Training script '''

from __future__ import print_function

import os
import argparse
import time

import numpy as np
import torch
from torch.utils.data import DataLoader
    
import deepclean as dc

t_start = time.time() # keep track of wall time

# torch.manual_seed(2147483647)
# np.random.seed(2147483647)
torch.set_default_tensor_type(torch.FloatTensor)

# Parse command line argument
def parse_cmd():
    
    parser = argparse.ArgumentParser(
        prog=os.path.basename(__file__), usage='%(prog)s [options]')
        
    # Add required arguments
    parser.add_argument(
        'config', help='path to setup config file',
        type=str, metavar='<path>')

    # Add optional arguments
    parser.add_argument(
        '--datadir', help="path to dataset directory", 
        dest="datadir", type=str, metavar='<path>')
    parser.add_argument(
        '--datadir-hoft', help="path to HOFT directory", 
        dest="datadir_hoft", type=str, metavar='<path>')
    parser.add_argument(
        '--datadir-witnesses', help="path to witnesses directory",
        dest="datadir_witnesses", type=str, metavar='<path>')
    parser.add_argument(
        '--outdir', help="path to output directory", 
        dest="outdir", type=str, metavar='<path>')
    parser.add_argument(
        '--aux-groups', help="aux groups",
        dest="aux_groups", nargs='+', type=str, metavar='<str>')
    parser.add_argument(
        '--batch-size', help='batch size',
        type=int, metavar='<int>')
    parser.add_argument(
        '--max-epochs', help='max epochs',
        type=int, metavar='<int>')
    parser.add_argument(
        '--use-gpu', help='run on gpu',
        type=bool, metavar='<bool>')

    # Parse as dictionary
    params = vars(parser.parse_args())
    
    datadir = params.pop('datadir')
    datadir_hoft = params.get('datadir_hoft')
    datadir_witnesses = params.get('datadir_witnesses')
    if datadir is not None:
        if (datadir_hoft is not None) or (datadir_witnesses is not None):
            raise ValueError('"datadir_hoft" or "datadir_witnesses" cannot be given'\
                             ' if "datadir" is given')
        params['datadir_hoft'] = datadir
        params['datadir_witnesses'] = datadir    
    return params

cmd_params = parse_cmd()

# Parse configuration file
# replace training key if command argument for that key is given
config = dc.io.parser.parse_config(
    cmd_params['config'], 
    ('training', 'timeseries', 'criterion', 'network', 'optimizer', 'scheduler')
)
training_params = config['training']
network_params = config['network']
timeseries_params = config['timeseries']
criterion_params = config['criterion']
optimizer_params = config['optimizer']
scheduler_params = config['scheduler']
dc.io.utils.replace_keys(training_params, cmd_params)

outdir = training_params['outdir']
os.makedirs(outdir, exist_ok=True)

# Setup hardware 
device = dc.nn.utils.get_device(training_params['use_gpu'])

# Load dataset into dataloader and preprocess
print('Loading dataset and preprocessing....')
train_ts, target_ts = dc.timeseries.get_timeseries(
    training_params['datadir_hoft'], training_params['datadir_witnesses'],
    timeseries_params, training_params['aux_groups']
)
train_loader = DataLoader(
    train_ts, batch_size=training_params['batch_size'], shuffle=False)
test_loader = DataLoader(
    target_ts, batch_size=training_params['batch_size'], shuffle=False)

# cache setting
network_params['input_dim'] = train_ts.n_channels - 1
dc.io.utils.save_setting(
    os.path.join(outdir, f'setting.bin'),
    timeseries_params, criterion_params, network_params, target_ts
)

# Get model, criterion and optimizers
print('Initializing network.....')
model = dc.nn.get_network(network_params, device)
print(model)

print('Initializing loss function.....')
criterion = dc.criterions.composite_psd.CompositePSDLoss(
    fs=train_ts.fs, device=device, **criterion_params)

print('Initializing optimizer and LR scheduler.....')
optimizer_params['params'] = model.parameters()
optimizer = dc.optimizers.get_optimizer(optimizer_params)
scheduler = dc.schedulers.get_scheduler(scheduler_params, optimizer)
print(optimizer)


# Start training
# initialize logger
logger = dc.logger.Logger(outdir=outdir, metrics=['loss'])

# training loop
t_train_start = time.time() # keep track of wall time
print('Start training')
dc.nn.utils.train(
    train_loader, test_loader, model, criterion, optimizer,
    scheduler=scheduler, max_epochs=training_params['max_epochs'], 
    logger=logger, device=device
)

# Print training time and wall time
train_time = time.time() - t_train_start
run_time = time.time() - t_start
print('Train time - %.4e secs' % train_time)
print('Wall time - %.4e secs' % run_time)
